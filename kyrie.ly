\version "2.14.2"
\pointAndClickOff

soprano = {
	\key a \major
	\time 4/4
	\clef treble
	\tempo 4  = 100
	
	a'4 b'8 cis'' cis''4 b'8 a'
	b'2 gis'
	
	b'4 cis''8 d'' d''4 cis''8 b'
	e''1
	
	d''4 cis''8 b' d''4 cis''8 b'
	a'1
	
	a'2 a'4 a'
	d''4 cis''8 b' a'2
	
	b'2 b'4 b'
	cis''4 b'8 cis''8 b'2
	
	d''2 d''4 d''
	e''2 e''2
	a'1

	a'4 b'8 cis'' cis''4 b'8 a'
	b'2 gis'
	
	b'4 cis''8 d'' d''4 cis''8 b'
	e''1
	
	d''4 cis''8 b' d''4 cis''8 b'
	a'1
}

alto = {
	\key a \major
	\time 4/4
	\clef treble
	e'4 e'8 e' e'4 e'8 e'
	e'1
	
	d'4 d'8 d' e'4 e'8 e'
	d'2 cis'2
	
	b4 b8 b a4 a8 a8
	cis'1
	
	e'2 cis'4 cis'
	fis'4 d'8 d'8 e'2
	
	gis'2 gis'4 gis'
	a'4 fis'8 fis'8 gis'2
	
	fis'2 a'4 a'
	gis'2 gis'
	e'1
	
	e'4 e'8 e' e'4 e'8 e'
	e'1
	
	d'4 d'8 d' e'4 e'8 e'
	d'2 cis'2
	
	b4 b8 b a4 a8 a8
	cis'1
}

tenor = {
	\key a \major
	\time 4/4
	\clef bass
	
	cis'4 cis'8 cis' b4 b8 b
	gis2 b2
	
	fis4 a8 a gis4 b8 b
	fis4 a gis b
	
	gis4 gis8 gis fis4 fis8 fis
	a1
	
	cis'2 a4 a4
	a4 a8 a a2
	
	gis2 b4 b
	a4 a8 a gis2
	
	a1
	gis1
	cis'1
	
	cis'4 cis'8 cis' b4 b8 b
	gis2 b2
	
	fis4 a8 a gis4 b8 b
	fis4 a gis b
	
	gis4 gis8 gis fis4 fis8 fis
	a1
}

bass = {
	\key a \major
	\time 4/4
	\clef bass
	
	a2 gis
	e1
	
	d2 e
	d2 e
	
	e2 d
	e1
	
	e1
	d2 a
	
	e1
	d2 e
	
	d1
	e1
	a1

	a2 gis
	e1
	
	d2 e
	d2 e
	
	e2 d
	e1	
}

\score {
	\new ChoirStaff <<
		\new Staff = "soprano" { \new Voice = "soprano" \set Staff.instrumentName = #"S" \soprano }
		\new Lyrics {
			\lyricsto "soprano" {
			Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- son Ky- ri- e e- le- i- son
			Chris- te e- le- i- - son Chris- te e- le- i- - son Chris- te e- le- i- son
			Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- son Ky- ri- e e- le- i- son
			}
		}	
		\new Staff = "alto" {\new Voice = "alto" \set Staff.instrumentName = #"A" \alto }
		\new Lyrics {
			\lyricsto "alto" {
			Ky- ri- e e- le- i- son Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- son
			Chris- te e- le- i- - son Chris- te e- le- i- - son Chris- te e- le- i- son
			Ky- ri- e e- le- i- son Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- son
			}
		}
		\new Staff = "tenor" {\new Voice = "tenor" \set Staff.instrumentName = #"T" \tenor }
		\new Lyrics {
			\lyricsto "tenor" {
			Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- so - - on Ky- ri- e e- le- i- son
			Chris- te e- le- i- - son Chris- te e- le- i- - son Chris- te son
			Ky- ri- e e- le- i- so- on Ky- ri- e e- le- i- so - - on Ky- ri- e e- le- i- son			
			}
		}		
		\new Staff = "bass" { \new Voice = "bass" \set Staff.instrumentName = #"B" \bass }
		\new Lyrics {
			\lyricsto "bass" {
				Ky- - ri e- - Ky- - ri- e  son 	
				Chris- te- e Chris- te- e Chris- te son
				Ky- - ri e- - Ky- - ri- e  son 	
			}
		}	
	>>
	\midi{}
	\layout{}
}

\header {
	title = "Missa"
	subtitle = "Kyrie"
	composer = "Stefan de Vries"
	tagline = "This work is licensed under a Creative Commons BY-SA license. See http://creativecommons.org/licenses/by-sa/3.0/"
}
